s30 activity

Objective 1
db.fruits.aggregate([
    { $match: {onSale: true}},
    { $count: "totalNumberOfFruitsOnSale"},
    { $out: "fruitsOnSale"}

])

Objective 2

db.fruits.aggregate([
    { $match: {stock: {$gte: 20}}},
     {$count: "fruitsWithMoreThan20Stocks"},
    {$out: "fruitsOnSaleWithMoreThan20Stocks"}
])

Objective 3

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id: "$supplier_id", averagePrice: {$avg: "$price"}}},
    {$out: "averagePriceOfFruitsOnSale"}
])

Objective 4

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id: "$supplier_id",maxPrice: {$max: "$price"} }},
    { $out: "highestPriceOffruitPersupplier"}
 ])

Objective 5

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id: "$supplier_id",minPrice: {$min: "$price"} }},
    { $out: "lowestPriceOffruitPersupplier"}
 ])